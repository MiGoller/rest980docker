# REST980 for Docker

This repository provides REST980 Docker images regulary built from [koalazak's rest980 repository](https://github.com/koalazak/rest980) to control supported iRobot Roomba series robots.
